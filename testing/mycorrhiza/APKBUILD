# Contributor: Umar Getagazov <umar@handlerug.me>
# Maintainer: Umar Getagazov <umar@handlerug.me>
pkgname=mycorrhiza
pkgver=1.8.2_git20220324
pkgrel=0
_commit="19d5a9e0574a5438cb0001bbf511dfcdf01cadb2"
pkgdesc="Lightweight wiki engine based on Git"
url="https://mycorrhiza.wiki/"
arch="all"
license="AGPL-3.0-only"
depends="git"
makedepends="go libcap"
install="$pkgname.pre-install"
subpackages="$pkgname-doc $pkgname-openrc"
options="!check net"  # no test suite, needs to download go modules
source="$pkgname-$_commit.tar.gz::https://github.com/bouncepaw/mycorrhiza/archive/$_commit.tar.gz
	mycorrhiza.initd
	mycorrhiza.confd
	"
builddir="$srcdir/$pkgname-$_commit"

build() {
	make
}

package() {
	make PREFIX="$pkgdir"/usr install
	setcap cap_net_bind_service=+ep "$pkgdir"/usr/bin/$pkgname
	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
}

sha512sums="
3f87f5b0686da9bc23d5e2666709123a0b58e0b73e1cf7e9f2a8c94d547f2e295648b4d1c2ea668f942f0e1c4f8038ef135b297926abe200ba34319171cbd50e  mycorrhiza-19d5a9e0574a5438cb0001bbf511dfcdf01cadb2.tar.gz
7e3f789189f190e934cb3279f037e7c6a2a93fae406cb93f2f2e7cc97eb764472a26b57d26f115a837761542c6538c632269cbb2d55aa75af33b91f5389f8e80  mycorrhiza.initd
a8cce20285b037371dae0ceae316d0dd21a22eb42150fbe08cd58ed83772b51896b5f9bc691f3c3ffb9155375d5d87832d66330a1167fd9d926d3658ccbe1e9f  mycorrhiza.confd
"
